/* global chrome replyRemoteValue listenRemoteValueRequest diffuseRemoteValue listenDiffuseValueRequest requestRemoteValue UI Config Util WS Broker Snowflake maybeChangeBackgroundPermission*/

// eslint-disable-next-line no-unused-vars
let log, dbg, snowflake, config, debug;

const DEFAULT_ENABLED = false;

const isServiceWorker = typeof chrome.offscreen !== 'undefined';
if (isServiceWorker) {
  const dummyFunction = () => {};
  // Ensure that the service worker wakes up and actually creates
  // the offscreen document (below).
  // Otherwise, when the browser is launched, none of the extension's
  // code is run, until the user opens the popup
  // (which starts the service worker).
  // See https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/issues/104#note_3070566
  //
  // TODO read up on offscreen document lifetime and ensure
  // that this covers all cases.
  chrome.runtime.onStartup.addListener(dummyFunction);
  chrome.runtime.onInstalled.addListener(dummyFunction);
}

(function () {
  'use strict';

  debug = true;

  async function init() {
    if (typeof chrome.offscreen === 'undefined') {
      await initOffscreen();
    } else {
      await initServiceWorker();
    }
  }

  async function initServiceWorker() {
    const storageDefaultValues = {
      "runInBackground": false,
      "snowflake-enabled": DEFAULT_ENABLED,
    };
    /** @type {undefined | typeof storageDefaultValues} */
    let storageCache = undefined;
    /** @type {Promise<typeof storageDefaultValues>} */
    const storageCacheP = chrome.storage.local.get(
      storageDefaultValues
    )
    .then((storage) => {
      storageCache = storage;
      return storageCache;
    });
    chrome.storage.onChanged.addListener(async (changes) => {
      const storageCache_ = storageCache || await storageCacheP;
      for (let [key, { newValue }] of Object.entries(changes)) {
        storageCache_[key] = newValue;
      }
    });

    async function onRemoteRequestValue(kind, name) {
      if (kind === "snowflake-preference") {
        let result = await chrome.storage.local.get([name]);
        replyRemoteValue(kind, name, result[name]);
      }
    }

    function onRemoteRequestValueWrapper(kind, name) {
      onRemoteRequestValue(kind, name);
    }

    listenRemoteValueRequest(onRemoteRequestValueWrapper);

    async function onRemoteDiffuseValue(kind, name, value) {
      if (kind === "snowflake-preference") {
        // Why need `storageCache` instead of just
        // `chrome.storage.local.get().then(maybeChangeBackgroundPermission)`?
        // Because if we use `then()`, then the
        // `chrome.permissions.request()` call is performed with
        // a greater delay after the user has clicked the toggle,
        // and we get an error:
        // > This function must be called during a user gesture
        // i.e. the "user gesture event" "times out".
        // See e.g. https://issues.chromium.org/issues/40238709
        //
        // And why not just `storageCache_ = await storageCacheP`?
        // Because of the same issue.
        // `await` always postpones the execution of an async function,
        // regardless of whether the promise is already resolved,
        // even if it's not a promise at all but an object itself.
        // Trying to use `storageCache` first effectively turns this function
        // into a synchronous one.
        //
        // So we want to never hit `await` here,
        // otherwise we'll get the error,
        // at least in the version 130 of Chromium.
        // TODO fix: but in reality, despite `chrome.storage.local.get()`
        // being very fast, this _can_ happen, e.g. if the user opened
        // the popup, then kept it open for a while until the service worker
        // shuts down. Then, when they toggle `runInBackground`,
        // we'll get the error.
        const storageCache_ = storageCache || await storageCacheP;

        // We do _not_ want to set `runInBackground` in storage here.
        // `maybeChangeBackgroundPermission()` will set it,
        // depending on whether the permission request was granted.
        if (name !== "runInBackground") {
          let obj = {};
          obj[name] = value;
          chrome.storage.local.set(obj);

          storageCache_[name] = value;
        }

        if (
          typeof SUPPORTS_WEBEXT_OPTIONAL_BACKGROUND_PERMISSION !== 'undefined'
          // eslint-disable-next-line no-undef
          && SUPPORTS_WEBEXT_OPTIONAL_BACKGROUND_PERMISSION
        ) {
          if (name === "snowflake-enabled") {
            maybeChangeBackgroundPermission(
              value,
              storageCache_["runInBackground"]
            );
          } else if (name === "runInBackground") {
            maybeChangeBackgroundPermission(
              storageCache_["snowflake-enabled"],
              value,
            );
          }
        }
      }
    }

    listenDiffuseValueRequest(onRemoteDiffuseValue);


    chrome.runtime.onMessage.addListener(onMessage);

    const existingContexts = await chrome.runtime.getContexts({
      contextTypes: ['OFFSCREEN_DOCUMENT']
    });

    /* Service worker may get terminated, the offscreen document will outlive it in this case  */
    if (existingContexts.length > 0) {
      return;
    }

    await chrome.offscreen.createDocument({
      url: 'offscreen.html',
      reasons: [chrome.offscreen.Reason.WEB_RTC],
      justification: 'Use WebRTC.',
    });


    // Keep other components alive
    sendMessage('snowflake-serviceworker-init', {});

    let consented = await chrome.storage.local.get(["snowflake-consented"]);
    if (consented["snowflake-consented"] !== true) {
      chrome.tabs.create({url: chrome.runtime.getURL("consent.html")});
    }
  }

  class WebExtOffscreenUI extends UI {
    constructor() {
      super();
      this.running = false;
    }

    checkNAT() {
      Util.checkNATType(config.datachannelTimeout).then((type) => {
        console.log("Setting NAT type: " + type);
        this.natType = type;
      }).catch((e) => {
        console.log(e);
      });
    }

    initNATType() {
      this.natType = "unknown";
      this.checkNAT();
      setInterval(() => {
        this.checkNAT();
      }, config.natCheckInterval);
    }

    tryProbe() {
      WS.probeWebSocket(config.defaultRelayAddr)
      .then(
        () => {
          this.missingFeature = false;
          this.setEnabled(true);
        },
        () => {
          log('Could not connect to bridge.');
          this.missingFeature = 'popupBridgeUnreachable';
          this.setEnabled(false);
        }
      );
    }

    postActive() {
      super.postActive();
      diffuseRemoteValue("snowflake-offscreen-status", "clientCount", this.clients);
      diffuseRemoteValue("snowflake-offscreen-status", "clientTotal", this.getTotal());
    }

    getTotal() {
      return this.stats.reduce((t, c) => t + c, 0);
    }
  }

  async function sendMessage(kind, data) {
    await chrome.runtime.sendMessage({
      kind: kind,
      data: data
    });
  }

  // eslint-disable-next-line no-unused-vars
  async function onMessage(message, sender, reply) {
    console.log('Received message: ' + message.kind);
  }


  async function initOffscreen() {

    chrome.runtime.onMessage.addListener(onMessage);

    config = new Config("webext");

    let ui = new WebExtOffscreenUI();

    let broker = new Broker(config);

    snowflake = new Snowflake(config, ui, broker);

    log = function (msg) {
      console.log('Snowflake: ' + msg);
    };

    dbg = log;

    log('== snowflake proxy ==');

    dbg('Contacting Broker at ' + broker.url);

    sendMessage('snowflake-offscreen-init', {});

    function replyOffscreenStatus(kind, name) {
      if (kind === "snowflake-offscreen-status") {
        if (name === "clientCount") {
          replyRemoteValue(kind, name, ui.clients);
          return;
        }
        if (name === "running") {
          replyRemoteValue(kind, name, ui.running);
          return;
        }
        if (name === "clientTotal") {
          replyRemoteValue(kind, name, ui.getTotal());
        }
        replyRemoteValue(kind, name, "");
      }
    }

    listenRemoteValueRequest(replyOffscreenStatus);

    function diffuseValueReceiver(kind, name, value) {
      if (kind === "snowflake-preference") {
        if (name === "snowflake-enabled") {
          if (value === true) {
            ui.initNATType();
            snowflake.beginServingClients();
            ui.running = true;
            diffuseRemoteValue("snowflake-offscreen-status", "running", true);
          } else {
            snowflake.disable();
            ui.running = false;
            diffuseRemoteValue("snowflake-offscreen-status", "running", false);
          }
        }
      }
    }

    listenDiffuseValueRequest(diffuseValueReceiver);

    let shouldAutoStart = await requestRemoteValue("snowflake-preference", "snowflake-enabled");
    console.log("Should auto start: " + shouldAutoStart);
    if (shouldAutoStart === true) {
      ui.initNATType();
      snowflake.beginServingClients();
      ui.running = true;
      diffuseRemoteValue("snowflake-offscreen-status", "running", true);
    }
  }

  // Initialize the extension
  init();
}());